// import logo from './logo.svg';
import "./App.css";

import { Routes, Route, Link } from "react-router-dom";

import Home from "./components/Home";
import About from "./components/About";
import ToDoPage from "./components/ToDoPage";
import CounterPage from "./components/CounterPage";

const routes = [
  { title: "homePage", component: Home, path: "/" },
  { title: "aboutPage", component: About, path: "/about" },
  { title: "toDoPage", component: ToDoPage, path: "/todopage" },
  { title: "counterPage", component: CounterPage, path: "/counter" },
];

function App() {
  return (
    <div className="container">
      <h2>React routes</h2>
      <nav>
        <ol>
          {routes.map((el, idx) => (
            <li key={idx + "_" + el.title}>
              <Link to={el.path}>{el.title}</Link>
            </li>
          ))}
        </ol>
      </nav>
      <hr />
      <Routes>
        {routes.map((el, idx) => (
          <Route
            exact
            key={idx + "-" + el.title}
            path={el.path}
            element={<el.component />}
          />
        ))}
      </Routes>
    </div>
  );
}

export default App;
