import React from "react";
import { connect } from "react-redux";
import * as actions from "../store/counter/counter_actions";

const CounterPage = (props) => {
  const { counter_st, increaseCounter, decreaseCounter } = props;
  return (
    <div>
      <h2>Counter Page</h2>
      <p>Number: {counter_st} </p>
      <button onClick={() => increaseCounter(3)}>Increase</button>
      <button onClick={() => decreaseCounter(2)}>Decrease</button>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    counter_st: state.counter_reduce,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    increaseCounter: (i) => dispatch(actions.increase_up(i)),
    decreaseCounter: (i) => dispatch(actions.decrease_down(i)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CounterPage);
