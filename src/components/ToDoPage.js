import React, { useState } from "react";
import { connect } from "react-redux";
import * as action from "../store/todos/todo_actions";
import { v4 as uuidv4 } from "uuid";

const ToDoPage = (props) => {
  const [oneTodo, setOneTodo] = useState({ name: "", details: "", id: "" });
  const [updateFlag, setUpdateFlag] = useState(false);
  const { todo_st, addTodo, deleteTodo, updateTodo } = props;
  const defaultTodo = { name: "", details: "", id: "" };
  const doUpDate = (t) => {
    setOneTodo(t);
    setUpdateFlag(true);
  };
  return (
    <div>
      <h2>ToDao Page</h2>
      <div className="form-group">
        <label htmlFor="tname">Name</label>
        <input
          type="text"
          className="form-control"
          name="tname"
          value={oneTodo.name}
          onChange={(e) => setOneTodo({ ...oneTodo, name: e.target.value })}
        />
      </div>
      <div className="form-group">
        <label htmlFor="tdetails">Details</label>
        <input
          type="text"
          className="form-control"
          name="tdetails"
          value={oneTodo.details}
          onChange={(e) => setOneTodo({ ...oneTodo, details: e.target.value })}
        />
        <br />
        {updateFlag ? (
          <button
            className="btn btn-warning"
            onClick={() => {
              updateTodo(oneTodo);
              setUpdateFlag(false);
            }}
          >
            Update
          </button>
        ) : (
          <button
            className="btn btn-primary"
            onClick={() => {
              const u_id = uuidv4();
              // setOneTodo({ ...oneTodo, id: u_id });
              addTodo(oneTodo, u_id);
              console.log(oneTodo);
              setOneTodo({ ...defaultTodo });
            }}
          >
            Add
          </button>
        )}
      </div>
      <div className="table-responsive">
        {todo_st.length > 0 ? (
          <table className="table table-hover">
            <thead>
              <tr>
                <th>Index</th>
                <th>Name</th>
                <th>Details</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {todo_st.map((it, idx) => (
                <tr key={idx + "_" + it.id}>
                  <td>{idx + 1}</td>
                  <td>{it.name}</td>
                  <td>{it.details}</td>
                  <td>
                    <button
                      className="btn btn-danger"
                      onClick={() => deleteTodo(it.id)}
                    >
                      Delete
                    </button>
                    <button
                      className="btn btn-warning"
                      onClick={() => doUpDate(it)}
                    >
                      Update
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : (
          <p style={{ color: "red" }}>Empty table</p>
        )}
      </div>
    </div>
  );
};
const mapStateToProps = (state) => {
  return { todo_st: state.todo_reduce };
};
const mapDispatchToProps = (dispatch) => {
  return {
    addTodo: (a, id) => {
      const u = a;
      u.id = id;
      dispatch(action.add_action(u));
    },
    deleteTodo: (i) => dispatch(action.delete_action(i)),
    updateTodo: (i) => dispatch(action.update_action(i)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ToDoPage);
