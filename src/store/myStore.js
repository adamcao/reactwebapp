import { createStore } from "redux";

import reduces from "./manyreducers";

const store = createStore(reduces);

export default store;
