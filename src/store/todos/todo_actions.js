import * as t from "./todo_type";

export const add_action = (u) => {
  return {
    type: t.ADD_TODO,
    payload: u,
  };
};

export const delete_action = (id) => {
  return {
    type: t.DELETE_TODO,
    payload: id,
  };
};

export const update_action = (u) => {
  return {
    type: t.UPDATE_TODO,
    payload: u,
  };
};

export const get_action = (id) => {
  return {
    type: t.ADD_TODO,
    payload: id,
  };
};

export const init_action = (uu) => {
  return {
    type: t.INIT_TODO,
    payload: uu,
  };
};
