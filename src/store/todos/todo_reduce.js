import * as tt from "./todo_type";

const defaultState = [
  { name: "shopping", details: "just go to shopping mall", id: 1 },
  { name: "camping", details: "state park camping", id: 2 },
];

const reduce = (state = defaultState, action) => {
  switch (action.type) {
    case tt.INIT_TODO:
      return [...action.payload];
    case tt.ADD_TODO:
      return [...state, action.payload];
    case tt.GET_TODO:
      return state.filter((i) => i.id === action.payload)[0];
    case tt.UPDATE_TODO:
      const new_s = state.map((i) => {
        if (i.id === action.payload.id) {
          i = { ...action.payload };
        }
        return i;
      });
      return [...new_s];
    case tt.DELETE_TODO:
      const new_d = state.filter((i) => i.id !== action.payload);
      return [...new_d];
    default:
      return state;
  }
};

export default reduce;
