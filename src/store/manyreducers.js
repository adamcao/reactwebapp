import { combineReducers } from "redux";

import todoReduce from "./todos/todo_reduce";
import counterReduce from "./counter/counter_reduce";
const rds = combineReducers({
  todo_reduce: todoReduce,
  counter_reduce: counterReduce,
});

export default rds;
