import * as c from "./conter_type";

const defaultState = 0;

const crds = (state = defaultState, action) => {
  switch (action.type) {
    case c.INCREASE_UP:
      return state + action.payload;
    case c.DECREASE_DOWN:
      return state - action.payload;
    default:
      return state;
  }
};
export default crds;
