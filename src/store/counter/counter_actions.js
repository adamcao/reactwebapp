import * as c from "./conter_type";

export const increase_up = (i) => {
  return {
    type: c.INCREASE_UP,
    payload: i,
  };
};

export const decrease_down = (i) => {
  return {
    type: c.DECREASE_DOWN,
    payload: i,
  };
};
